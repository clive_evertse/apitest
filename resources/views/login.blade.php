<!DOCTYPE html>
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<title>Register</title>
		</head>
		<body>
		 	<div class="container">
        		<div class="row">
            		<div class="col-md-10 col-md-offset-1">               
						<div class="content">
							<form>
								@if(isset($message))									
										<div class="alert alert-warning">
		  									{{$message}}
										</div>									
								@endif
				  				<div class="form-group">
									<input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Username">   
  								</div>
  								<div class="form-group">
    								<input type="password" class="form-control" id="password" placeholder="Password">
  								</div>
  								<button type="submit" class="btn btn-primary">Log In</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</body>
	</html>