<!DOCTYPE html>
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<title>Edit Profile</title>
		</head>
		<body>
			<div class="content">
				<form>
  					<div class="form-group">
						<input type="text" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Full Name">   
  					</div>
    				<div class="form-group">
						<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">    
  					</div>
  					<div class="form-group">
						<input type="text" class="form-control" id="contactNumber" aria-describedby="emailHelp" placeholder="Contact Number">   
  					</div>
  					<div class="form-group">
    					<input type="password" class="form-control" id="password" placeholder="Password">
  					</div>
  					<div class="form-group">
    					<input type="password" class="form-control" id="confirmPassword" placeholder="Password">
  					</div>
  					<button type="submit" class="btn btn-success">Link My Strava Account</button> </br></br>
  					<button type="submit" class="btn btn-primary">Register</button>
				</form>
			</div>
		</body>
	</html>