<!DOCTYPE html>
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<title>Register</title>
		</head>
		<body>
		 	<div class="container">
        		<div class="row">
            		<div class="col-md-10 col-md-offset-1">               
						<div class="content">
							<form>
								@if(isset($messages))
									@foreach($messages as $message)
										<div class="alert alert-warning">
		  									{{$message}}
										</div>
									@endforeach
								@endif
				  				<div class="form-group">
									<input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Username">   
  								</div>
  								<div class="form-group">
									<input type="text" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Full Name">   
  								</div>
    							<div class="form-group">
									<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">    
  								</div>
  								<div class="form-group">
									<input type="text" class="form-control" id="contactNumber" aria-describedby="emailHelp" placeholder="Contact Number">   
  								</div>
  								<div class="form-group">
    								<input type="password" class="form-control" id="password" placeholder="Password">
  								</div>
  								<div class="form-group">
    								<input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password">
  								</div>
  								<button type="submit" class="btn btn-primary">Continue</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</body>
	</html>