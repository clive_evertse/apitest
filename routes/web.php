<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('register',['as' => 'engena.register', 'uses' => 'EngenaController@register']);

Route::any('loginUser',['as' => 'engena.loginUser', 'uses' => 'EngenaController@loginUser']);
Route::any('registerUser',['as' => 'engena.registerUser', 'uses' => 'EngenaController@registerUser']);
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::any('logout', function(){
		Auth::logout();
});
