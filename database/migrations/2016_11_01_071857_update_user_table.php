<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function ($table) {
            $table->renameColumn('name', 'fullname');
            $table->string('mobile')->after('email');
            $table->string('wechat_id')->after('mobile');
            $table->string('engena_token')->after('wechat_id');
            $table->string('strava_token')->after('engena_token');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
