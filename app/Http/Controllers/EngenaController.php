<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Api\ApiTest;
use Validator;
use View;
use Config;
use Cache;
use Illuminate\Support\Facades\Auth;

class EngenaController extends Controller
{
    public function loginUser(){

      $apiTest = new ApiTest();
      $response = $apiTest->loginUser('clivejt','Krusty_66+');
      $response = json_decode(json_encode($response), true);
      //print_r($response);die();
      if(isset($response['token'])){
        echo '<pre>';
          echo 'token => '.$response['token'];
        echo '</pre>'; 
      }
      elseif($response['status_code'] == 401 || $response['status_code'] == 422){
          $message = $response['message'];
          return view('login',array('message' => $message));
      }       
    }

    public function registerUser(){
    	$apiTest = new ApiTest();
      $response = $apiTest->registerUser('clivejt14','Krusty_66+','Clive Evertse','clivejt14@gmail.com','0795496114');
      $response = json_decode(json_encode($response), true);
      if(isset($response['errors'])){
        
        if(isset($response['errors']['username'])){
          $messages[] = $response['errors']['username'][0];
        }
        if(isset($response['errors']['mobile'])){
          $messages[]  = $response['errors']['mobile'][0];
        }
        if(isset($response['errors']['email'])){
          $messages[] = $response['errors']['email'][0];
        }

        return view('register',array('messages' => $messages));
      }
      elseif(isset($response['status'])){
          $messages[] = "Please verify your email to validate your account";
          return view('register',array('messages' => $messages));
      }
    }

    public function register(){
    	return view('register');
    }
}